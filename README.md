# Similar images search

This is a student's project on DL: task of finding similar images in a dataset.
Three approaches are tested: 
1) FASTERRCNN_RESNET50_FPN + KNN
2) CLIP + KNN
3) CLIP text search + KNN

## How to build
install dependencies
```bash
pip install -r requirements.txt
```
run:
```bash
FLASK_APP=app.py flask run --host 0.0.0.0
```
## Where to get pretrained model and dataset
Model weights: https://drive.google.com/file/d/12ePSQ_rJtO78YeCazLdhZubeUQfCBq2j/view?usp=sharing

Model was trained on 10k images from the COCO-dataset. 
Dataset (10k images): https://drive.google.com/file/d/1_6fnbEtu3s52KHaXJJJeRtCA8fwFr3rg/view?usp=sharing

## License

The mighty MIT license. Please check `LICENSE` for more details.
