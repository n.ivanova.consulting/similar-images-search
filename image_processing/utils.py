import os
import pickle as pk

import numpy as np
import torch
from PIL import Image
from torchvision import transforms
from tqdm import tqdm


def extract_id(s: str):
    s = s[:-4]
    first_nonzero = -1
    for i, c in enumerate(s):
        if c != "0":
            first_nonzero = i
            break
    s = s[first_nonzero:]
    return int(s)


def read_img_file(f):
    img = Image.open(f)
    if img.mode != "RGB":
        img = img.convert("RGB")
    return img


transform_val = transforms.Compose(
    [
        transforms.Resize((416, 416)),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    ]
)

def get_features(img, model):
    device = "cuda:0" if torch.cuda.is_available() else "cpu"
    hook_features = []

    def save_features(mod, inp, outp):
        hook_features.append(outp.data)

    layer_to_hook = "roi_heads.box_head.fc7"
    for name, layer in model.named_modules():
        if name == layer_to_hook:
            layer.register_forward_hook(save_features)

    preprocessed_img = transform_val(img).unsqueeze(0).to(device)
    _ = model(
        preprocessed_img,
    )
    image_features = hook_features[0][0].cpu().detach().numpy()

    image_features /= np.linalg.norm(image_features)
    image_features = image_features.flatten()
    hook_features = []
    
    return image_features


def get_features_clip(image, model, preprocess):
    device = "cuda:0" if torch.cuda.is_available() else "cpu"
    preprocessed_img = preprocess(image).unsqueeze(0).to(device)
    with torch.no_grad():
        image_features = model.encode_image(preprocessed_img)
        image_features /= image_features.norm(dim=-1, keepdim=True)
    image_features = image_features.squeeze()
    return image_features.cpu().numpy()

def generate_model_features(
    images_path,
    pickle_file="resnet_image_features.pkl",
    use_clip=False,
    model=None,
    preprocess=None,
):
    device = "cuda:0" if torch.cuda.is_available() else "cpu"
    all_image_features = []
    image_filenames = os.listdir(images_path)
    image_ids = set(map(extract_id, image_filenames))
    try:
        all_image_features = pk.load(open(pickle_file, "rb"))
    except (OSError, IOError) as e:
        print("file_not_found")

    def exists_in_all_image_features(image_id):
        for image in all_image_features:
            if image["image_id"] == image_id:
                return True
        return False

    def exists_in_image_folder(image_id):
        if image_id in image_ids:
            return True
        return False

    def sync_image_features():
        for_deletion = []

        for i in range(len(all_image_features)):
            if not exists_in_image_folder(all_image_features[i]["image_id"]):
                print("deleting " + str(all_image_features[i]["image_id"]))
                for_deletion.append(i)

        for i in reversed(for_deletion):
            del all_image_features[i]

    sync_image_features()
   
    for image_filename in tqdm(image_filenames):
        
        image_id = extract_id(image_filename)
        if exists_in_all_image_features(image_id):
            continue
        open_image = Image.open if use_clip else read_img_file
        img_arr = open_image(os.path.join(images_path, image_filename))
        image_features = None
        if use_clip:
            image_features = get_features_clip(img_arr, model, preprocess)
        else:
            image_features = get_features(img_arr, model)
        all_image_features.append({"image_id": image_id, "features": image_features})
    pk.dump(all_image_features, open(pickle_file, "wb"))
