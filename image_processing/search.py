import io
import os
import pickle as pk
from base64 import b64encode

import numpy as np
# System modules
import torchvision
# PyTorch and image processing
from PIL import Image
# Scikit-learn algorithms and metrics
from sklearn.neighbors import NearestNeighbors

from image_processing.utils import *

import hnswlib
import clip

IMAGES_PATH = "/home/perminov/ivanova_data/coco_dataset/small_coco_dataset"
CLIP_PICKLE_FILE = "image_processing/clip_image_features.pkl"

def image_search(image_bytes, pickle_file, model, preprocess=None, use_clip=False, n_neighbors=8):
    query_image_pillow = Image.open(io.BytesIO(image_bytes))
    
    if use_clip:
        query_image_features = get_features_clip(query_image_pillow, model, preprocess)
    else:
        query_image_features = get_features(query_image_pillow, model)
    image_features = pk.load(open(pickle_file, "rb"))
    features = []

    for image in image_features:
        features.append(np.array(image["features"]))
    features = np.array(features)
    features = np.squeeze(features)

    path = IMAGES_PATH
    knn = NearestNeighbors(
        n_neighbors=n_neighbors, algorithm="brute", metric="euclidean"
    )
    knn.fit(features)
    file_names = os.listdir(path)
   
    indices = knn.kneighbors([query_image_features], return_distance=False)

    found_images = []

    for x in indices[0]:
        buf = io.BytesIO()
        buf.seek(0)
        image = Image.open(os.path.join(path, file_names[x]))
        image.save(buf, "PNG")
        buf.seek(0)
        found_images.append(b64encode(buf.read()).decode("utf-8"))
    return found_images

def text_search(textQuery="elephants", clip_pickle_file=None, model=None, n_neighbors=10):
    device = "cuda:0" if torch.cuda.is_available() else "cpu"
    if model is None:
        model, _ = clip.load("ViT-B/32")
        model = model.to(device)
        model.eval()
    if clip_pickle_file is None:
        clip_pickle_file = CLIP_PICKLE_FILE
    if len(textQuery) == 0:
        textQuery = "elephants"
    image_features = pk.load(open(clip_pickle_file, "rb"))
    features = []
    for image in image_features:
        features.append(np.array(image["features"]))
    features = np.array(features)
    features = np.squeeze(features)

    path = IMAGES_PATH
    file_names = os.listdir(IMAGES_PATH)
    dim = 512
    index = hnswlib.Index(space="l2", dim=dim)
    index.init_index(max_elements=10000, ef_construction=100, M=16)
    index.add_items(features)

    text_tokenized = clip.tokenize([textQuery]).to(device)

    with torch.no_grad():
        text_features = model.encode_text(text_tokenized)
        text_features /= text_features.norm(dim=-1, keepdim=True)

    labels, _ = index.knn_query(text_features.cpu().numpy(), k=n_neighbors)

    labels = labels[0]
    found_images = []

    for idx in labels:
        buf = io.BytesIO()
        buf.seek(0)
        image = Image.open(os.path.join(path, file_names[idx]))
        image.save(buf, "PNG")
        buf.seek(0)
        found_images.append(b64encode(buf.read()).decode("utf-8"))

    return found_images
