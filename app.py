import os

import clip
import torch
import torchvision
from flask import (Flask, Response, jsonify, redirect, render_template,
                   request, stream_with_context)

import image_processing.search as rs
from image_processing.utils import generate_model_features

IMAGES_PATH = "/home/perminov/ivanova_data/coco_dataset/small_coco_dataset"
PICKLES = "image_processing"

app = Flask(__name__)

device = "cuda:0" if torch.cuda.is_available() else "cpu"
# device = "cpu"
resnet_model = torchvision.models.detection.fasterrcnn_resnet50_fpn()
resnet_model.load_state_dict(torch.load("model_similarity_search_10k_10e.pth"))
resnet_model = resnet_model.to(device)
resnet_model.eval()

clip_model, clip_preprocess = clip.load("ViT-B/32")


generate_model_features(
    IMAGES_PATH,
    os.path.join(PICKLES, "resnet_image_features.pkl"),
    model=resnet_model,
)

torch.cuda.empty_cache()
import gc
gc.collect()

generate_model_features(
    IMAGES_PATH,
    os.path.join(PICKLES, "clip_image_features.pkl"),
    model=clip_model,
    preprocess=clip_preprocess,
    use_clip=True,
)


@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "POST":
        print(f"\n*** {request.form} ***\n")
        file = request.files["image"]
        text_query = request.form["textquery"]
        model_id = request.form["model"]
        
        image_bytes = file.read()
        found_images = []
        if model_id == "1":
            found_images = rs.image_search(
                image_bytes=image_bytes,
                pickle_file=os.path.join(PICKLES, "resnet_image_features.pkl"),
                model=resnet_model
            )
        elif model_id == "2":
            found_images = rs.image_search(
                image_bytes=image_bytes,
                pickle_file=os.path.join(PICKLES, "clip_image_features.pkl"),
                model=clip_model,
                preprocess=clip_preprocess,
                use_clip=True
            )
        else:
            found_images = rs.text_search(
                text_query,
                os.path.join(PICKLES, "clip_image_features.pkl"),
                clip_model
            )
        return render_template(
            "result.html",
            found_images=list(zip(range(len(found_images)), found_images)),
        )

    return render_template("index.html")
